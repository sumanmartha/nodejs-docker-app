# NodeJS docker application 

## Context

Docker applicaiton will show 'Hello World' in web browser

### build docker image

docker build -t sumanmartha/nodejs-docker-app .

## Run the application

docker run -p 35460:8080 -d sumanmartha/nodejs-docker-app

## Explore in web browser

http://localhost:35460


## Creating Kubernetes application

Create application with commands:
	- `kubectl apply -f hello-app.yaml`
	- `kubectl apply -f hello-service.yaml`

This repository contains:

- `server.js` contains the HTTP server implementation. It responds to all HTTP
  requests with a  `Hello, world!` response.
- `Dockerfile` is used to build the Docker image for the application.
- `bitbucket-pipelines.yml` is a bitibucket pipeline for deployment(CICD).
